<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
		\Gate::define('cms', function ($user) {
			return ($user->roles->first()->name === 'admin' ||
				$user->roles->first()->name === 'member' ||
				$user->roles->first()->name === 'admincompany' ||
				$user->roles->first()->name === 'coordinator' ||
				$user->roles->first()->name === 'runner' ||
				$user->roles->first()->name === 'pemberi_perintah' ||
				$user->roles->first()->name === 'pemberi_tugas' ||
				$user->roles->first()->name === 'pelaksana' ||
				$user->roles->first()->name === 'pembagi_tugas'  );
		});
		\Gate::define('admin', function ($user) {
			return ($user->roles->first()->name === 'admin');
		});
		\Gate::define('cms', function ($user) {
			return ($user->roles->first()->name === 'admin' ||
				$user->roles->first()->name === 'member' ||
				$user->roles->first()->name === 'admincompany' ||
				$user->roles->first()->name === 'coordinator' ||
				$user->roles->first()->name === 'runner' ||
				$user->roles->first()->name === 'pemberi_perintah' ||
				$user->roles->first()->name === 'pemberi_tugas' ||
				$user->roles->first()->name === 'pelaksana' ||
				$user->roles->first()->name === 'pembagi_tugas'  );
		});
		\Gate::define('admin', function ($user) {
			return ($user->roles->first()->name === 'admin');
		});
		\Gate::define('cms', function ($user) {
			return ($user->roles->first()->name === 'admin' ||
				$user->roles->first()->name === 'member' ||
				$user->roles->first()->name === 'admincompany' ||
				$user->roles->first()->name === 'coordinator' ||
				$user->roles->first()->name === 'runner' ||
				$user->roles->first()->name === 'pemberi_perintah' ||
				$user->roles->first()->name === 'pemberi_tugas' ||
				$user->roles->first()->name === 'pelaksana' ||
				$user->roles->first()->name === 'pembagi_tugas'   );
		});
		\Gate::define('admin', function ($user) {
			return ($user->roles->first()->name === 'admin');
		});
		\Gate::define('cms', function ($user) {
			return ($user->roles->first()->name === 'admin' ||
				$user->roles->first()->name === 'member' ||
				$user->roles->first()->name === 'admincompany' ||
				$user->roles->first()->name === 'coordinator' ||
				$user->roles->first()->name === 'runner' ||
				$user->roles->first()->name === 'pemberi_perintah' ||
				$user->roles->first()->name === 'pemberi_tugas' ||
				$user->roles->first()->name === 'pelaksana' ||
				$user->roles->first()->name === 'pembagi_tugas'   );
		});
		\Gate::define('admin', function ($user) {
			return ($user->roles->first()->name === 'admin'  );
		});

        //
    }
}
