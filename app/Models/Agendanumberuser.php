<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendanumberuser extends Model
{
    public $table = "agendanumberusers";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'=agenda_number_id',
		'user_id',

    ];

    public static $rules = [
        // create rules
    ];

    // Agendanumberuser 
}
