<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendanumber extends Model
{
    public $table = "agendanumbers";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'=id',
		'name',
		'counter',
		'suffix',

    ];

    public static $rules = [
        // create rules
    ];

    // Agendanumber 
}
