<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipient extends Model
{
    public $table = "recipients";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'user_id',
		'lt_letter_id',
		'is_cc',
		'is_checker',
		'has_been_read',
		'has_finished',
		'trashed',
		'trashed_at',
		'approved',
		'reading_date',

    ];

    public static $rules = [
        // create rules
    ];

    // Recipient 
}
