<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Letterattachment extends Model
{
    public $table = "letterattachments";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
// Letterattachment table data
    	"name", "path", "description", "user_id"
    ];

    public static $rules = [
        // create rules
    ];

    // Letterattachment 

    public function getFileUrlAttribute($value)
    {
        // cek file ada
        if (file_exists(storage_path('app/'.$this->path))) { 
            return url(str_replace('public/', 'storage/', $this->path));
        }
        
        return "not found";
    }
}
