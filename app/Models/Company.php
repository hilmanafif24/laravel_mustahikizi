<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Intervention\Image\ImageManagerStatic as InterventionImage;
use Storage;
use Grafite\Cms\Services\AssetService;
use Config;

class Company extends Model
{
    public $table = "companies";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'name',
		'tax',
		'reg',
		'phone',
		'fax',
		'address1',
		'address2',
		'city',
		'province',
		'zip',
		'country',
		'logo',
		'timezone',
		'currency',

    ];

    public static $rules = [
        // create rules
    ];

    // Company 
    public function users()
    {
        return $this->hasMany(User::class);
    }

    public static function findByName($name)
    {
        return User::where('name', $name)->firstOrFail();
    }

    public function getLogoImageUrlAttribute($value)
    {
        // cek image ada
        if (file_exists(storage_path('app/'.$this->logo))) { 
            return url(str_replace('public/', 'storage/', $this->logo));
        }


        // jika tidak ada pake image not found
        $imagePath = app(AssetService::class)->generateImage('File Not Found');

        $image = InterventionImage::make($imagePath)->resize(config('cms.preview-image-size', 800), null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return (string) $image->encode('data-url');
    }

}
