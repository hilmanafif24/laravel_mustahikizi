<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Letterattachmentletter extends Model
{
    public $table = "letterattachmentletters";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'lt_attachment_id',
		'lt_letter_id',

    ];

    public static $rules = [
        // create rules
    ];

    // Letterattachmentletter 
}
