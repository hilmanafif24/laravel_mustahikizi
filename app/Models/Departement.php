<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Departement extends Model
{
    public $table = "departements";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'code',
		'name',
		' parent_id',
		' position',
		'status',

    ];

    public static $rules = [
        // create rules
    ];

    // Departement 
}
