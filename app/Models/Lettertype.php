<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lettertype extends Model
{
    public $table = "lettertypes";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
// Lettertype table data
    	'id',
		'name',
		'label',
    ];

    public static $rules = [
        // create rules
    ];

    // Lettertype 
}
