<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Anggaran extends Model
{
    public $table = "anggarans";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'name',
		'counter',
		'suffix',

    ];

    public static $rules = [
        // create rules
    ];

    // Anggaran 
}
