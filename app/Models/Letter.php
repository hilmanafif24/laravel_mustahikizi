<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
    public $table = "letters";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'letter_no',
		'agenda_no',
		'lt_source_id',
		'lt_type_id',
		'lt_money',
		'lt_material',
		'draft_no',
		'letterout_no',
		'letter_date',
		'received_date',
		'input_date',
		'sender',
		'recipient',
		'subject',
		'appendix',
		'body',
		'letterhead',
		'greeting',
		'signature',
		'has_read',
		'lt_level_id',
		'lt_priority_id',
		'lt_type_id',
		'lt_name_id',
		'parent_id',
		'finishing_date',
		'note',
		'sender_user_id',
		'lt_sender_user_id',
		'locked',
		'sender_trashed',
		'sender_trashed_at',
		'letter_out',
		'is_draft',
		'edit_date',
		'created_at'

    ];

    public static $rules = [
        // create rules
    ];

    // Letter 
}
