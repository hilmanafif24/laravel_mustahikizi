<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lettersource extends Model
{
    public $table = "lettersources";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
		'id',
		'name',
		'label',

    ];

    public static $rules = [
        // create rules
    ];

    // Lettersource 
}
