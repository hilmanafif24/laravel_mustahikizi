<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::user()->can('admin')) {
            return true;
        }elseif (Auth::user()->roles[0]->name == 'admincompany') {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // return User::$rules;
        return [
             'nipp' => 'required|unique:users',
            'handphone' => 'required|unique:users',
            'email' => 'required|unique:users',
        ];
    }
}
