<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AgendanumberService;
use App\Http\Requests\AgendanumberCreateRequest;
use App\Http\Requests\AgendanumberUpdateRequest;

class AgendanumbersController extends Controller
{
    public function __construct(AgendanumberService $agendanumber_service)
    {
        $this->service = $agendanumber_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $agendanumbers = $this->service->paginated();
        return view('agendanumbers.index')->with('agendanumbers', $agendanumbers);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $agendanumbers = $this->service->search($request->search);
        return view('agendanumbers.index')->with('agendanumbers', $agendanumbers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agendanumbers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\AgendanumberCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgendanumberCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('agendanumbers.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('agendanumbers.index'))->withErrors('Failed to create');
    }

    /**
     * Display the agendanumber.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agendanumber = $this->service->find($id);
        return view('agendanumbers.show')->with('agendanumber', $agendanumber);
    }

    /**
     * Show the form for editing the agendanumber.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agendanumber = $this->service->find($id);
        return view('agendanumbers.edit')->with('agendanumber', $agendanumber);
    }

    /**
     * Update the agendanumbers in storage.
     *
     * @param  App\Http\Requests\AgendanumberUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgendanumberUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the agendanumbers from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('agendanumbers.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('agendanumbers.index'))->withErrors('Failed to delete');
    }
}
