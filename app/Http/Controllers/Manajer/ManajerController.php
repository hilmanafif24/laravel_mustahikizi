<?php

namespace App\Http\Controllers\Manajer;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Letter;
use App\Models\Letterattachmentletter;
use App\Models\Letterattachment;

use Auth;
use Illuminate\Support\Facades\Schema;


class ManajerController extends Controller
{
   
    public function index()
    {
    	 return redirect('/cms/dashboard');
    }

    public function spk()
    {

        $user_id = Auth::user()->id;
        $letters = Letter::where('sender_user_id', $user_id)->where('lt_type_id', 1)->orderBy('id', 'DESC')
        ->paginate(25);
        return view('manajer.spk')->with('letters', $letters);
    }

    public function spk_show($id)
    {
         $letter = Letter::find($id);
         
         $attachment = Letterattachmentletter::where('lt_letter_id', $id)->first();

         if($attachment != null){
            $attachment_url = Letterattachment::find($attachment->lt_attachment_id);
        }else{
            $attachment_url = "not found";
        }
         
         
         return view('manajer.showspk')->with('letter', $letter)->with('attachment', $attachment_url);

    }

    public function spk_add()
    {
        return view('manajer.spkcreate');
    }

    public function spk_store(Request $request)
    {
        $letters = new Letters;

        $letters->name = $request->name;

        $letters->save();


    }

    public function laporan()
    {
        $user_id = Auth::user()->id;
        $letters = Letter::where('recipient', $user_id)->where('lt_type_id', 3)
        ->paginate(25);
        return view('manajer.laporan')->with('letters', $letters);
      
    }

    public function laporan_show($id)
    {
        $letter = Letter::find($id);
        $letter->has_read = 1;
        $letter->save();

        $attachment = Letterattachmentletter::where('lt_letter_id', $id)->first();

        if($attachment != null){
            $attachment_url = Letterattachment::find($attachment->lt_attachment_id);
        }else{
            $attachment_url = "not found";
        }

         return view('manajer.showlaporan')->with('letter', $letter)->with('attachment', $attachment_url);
      
    }
}
