<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use App\Http\Requests\CompanyCreateRequest;
use App\Http\Requests\CompanyUpdateRequest;
use Grafite\Cms\Services\FileService;
use Storage;
use Config;

class CompaniesController extends Controller
{
    public function __construct(CompanyService $company_service)
    {
        $this->service = $company_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companies = $this->service->paginated();
        return view('companies.index')->with('companies', $companies);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $companies = $this->service->search($request->search);
        return view('companies.index')->with('companies', $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\CompanyCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyCreateRequest $request)
    {
        
        // Jika ada file, maka sistem menyimpan file gambar pada folder storage 
        if ($request->logo2) {
            $file = request()->file('logo2');
            $path = app(FileService::class)->saveFile($file, 'public/images', [], true);   
        }

        // mengambil lokasi file yang sudah tersimpan (public/images/nama_file)
        $request->merge([
                'logo'=> $path['name'],
        ]);

        // Menyimpan data form ke dalam database, kecuali token dan logo (karena sudah diperbaharui lokasi filennya)
        $result = $this->service->create($request->except('_token', 'logo2'));

        // Redirect
        if ($result) {
            return redirect(route('companies.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('companies.index'))->withErrors('Failed to create');
    }

    /**
     * Display the company.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = $this->service->find($id);
        return view('companies.show')->with('company', $company);
    }

    /**
     * Show the form for editing the company.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // mengambil data
        $company = $this->service->find($id);
        // mengambil lokasi gambar
        $image_url = $company->getLogoImageUrlAttribute($company); 
        return view('companies.edit')->with('company', $company)->with('image', $image_url);
    }

    /**
     * Update the companies in storage.
     *
     * @param  App\Http\Requests\CompanyUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request, $id)
    {
        if ($request->logo2) {
            //hapus dulu image lama
            $image = $this->service->find($id);

            if (is_file(storage_path($image->logo))) {
                Storage::delete($image->logo);
            } else {
                Storage::disk(Config::get('cms.storage-location', 'local'))->delete($image->logo);
            }

            // baru upload image baru
            $file = request()->file('logo2');
            $path = app(FileService::class)->saveFile($file, 'public/images', [], true); 
            $request->merge([
                'logo'=> $path['name'],
            ]);  
        }
      
        $result = $this->service->update($id, $request->except('_token', 'logo2'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the companies from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = $this->service->find($id);

        if (is_file(storage_path($image->logo))) {
            Storage::delete($image->logo);
        } else {
            Storage::disk(Config::get('cms.storage-location', 'local'))->delete($image->logo);
        }

        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('companies.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('companies.index'))->withErrors('Failed to delete');
    }
}
