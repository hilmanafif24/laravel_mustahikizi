<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\User;
use Illuminate\Foundation\Auth\ResetsPasswords;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide this functionality to your appliations.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Check user's role and redirect user based on their role
     * @return
     */
    public function authenticated()
    {
        if (auth()->user()->hasRole('admin')) {
            return redirect('/admin/dashboard');
        }

        if (auth()->user()->hasRole('member')) {
            return redirect('/companies/');
        }

        return redirect('dashboard');
    }

    public function username()
    {
        return 'nipp';
    }

    public function showForm()
    {
        return view('auth.login');
    }

    public function otp(Request $request)
    {
        $handphone= $request->handphone;
        $user = User::select('id','handphone')->where('handphone', $handphone)->first();
        if($user){

            // Random New Password
            $password = rand(1000,9999);
            $user->password = bcrypt($password);
            $user->save();

            // Send SMS with Password 
            if($user->handphone == '83100180252' || $user->handphone == '895352394642'){
                $nexmo = app('Nexmo\Client');
                $nexmo->message()->send([
                    'to'   => '62'.$user->handphone,
                    'from' => "NEXMO",
                    'text' => "Nomor Anda ".$password." "
                ]);    
            }
            
            // Fase Dev. Password Di
            return view('auth.otp')->with('handphone', $user->handphone)->with('password', $password);
        }else{
            return view('auth.phone');
        }
    }
}