<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LettertypeService;
use App\Http\Requests\LettertypeCreateRequest;
use App\Http\Requests\LettertypeUpdateRequest;

class LettertypesController extends Controller
{
    public function __construct(LettertypeService $lettertype_service)
    {
        $this->service = $lettertype_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lettertypes = $this->service->paginated();
        return view('lettertypes.index')->with('lettertypes', $lettertypes);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $lettertypes = $this->service->search($request->search);
        return view('lettertypes.index')->with('lettertypes', $lettertypes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lettertypes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\LettertypeCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LettertypeCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('lettertypes.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('lettertypes.index'))->withErrors('Failed to create');
    }

    /**
     * Display the lettertype.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lettertype = $this->service->find($id);
        return view('lettertypes.show')->with('lettertype', $lettertype);
    }

    /**
     * Show the form for editing the lettertype.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lettertype = $this->service->find($id);
        return view('lettertypes.edit')->with('lettertype', $lettertype);
    }

    /**
     * Update the lettertypes in storage.
     *
     * @param  App\Http\Requests\LettertypeUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LettertypeUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the lettertypes from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('lettertypes.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('lettertypes.index'))->withErrors('Failed to delete');
    }
}
