<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AnggaranService;
use App\Http\Requests\AnggaranCreateRequest;
use App\Http\Requests\AnggaranUpdateRequest;

class AnggaransController extends Controller
{
    public function __construct(AnggaranService $anggaran_service)
    {
        $this->service = $anggaran_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $anggarans = $this->service->paginated();
        return view('anggarans.index')->with('anggarans', $anggarans);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $anggarans = $this->service->search($request->search);
        return view('anggarans.index')->with('anggarans', $anggarans);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('anggarans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\AnggaranCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnggaranCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('anggarans.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('anggarans.index'))->withErrors('Failed to create');
    }

    /**
     * Display the anggaran.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $anggaran = $this->service->find($id);
        return view('anggarans.show')->with('anggaran', $anggaran);
    }

    /**
     * Show the form for editing the anggaran.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggaran = $this->service->find($id);
        return view('anggarans.edit')->with('anggaran', $anggaran);
    }

    /**
     * Update the anggarans in storage.
     *
     * @param  App\Http\Requests\AnggaranUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnggaranUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the anggarans from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('anggarans.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('anggarans.index'))->withErrors('Failed to delete');
    }
}
