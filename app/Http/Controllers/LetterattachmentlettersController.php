<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LetterattachmentletterService;
use App\Http\Requests\LetterattachmentletterCreateRequest;
use App\Http\Requests\LetterattachmentletterUpdateRequest;

class LetterattachmentlettersController extends Controller
{
    public function __construct(LetterattachmentletterService $letterattachmentletter_service)
    {
        $this->service = $letterattachmentletter_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $letterattachmentletters = $this->service->paginated();
        return view('letterattachmentletters.index')->with('letterattachmentletters', $letterattachmentletters);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $letterattachmentletters = $this->service->search($request->search);
        return view('letterattachmentletters.index')->with('letterattachmentletters', $letterattachmentletters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('letterattachmentletters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\LetterattachmentletterCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LetterattachmentletterCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('letterattachmentletters.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('letterattachmentletters.index'))->withErrors('Failed to create');
    }

    /**
     * Display the letterattachmentletter.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $letterattachmentletter = $this->service->find($id);
        return view('letterattachmentletters.show')->with('letterattachmentletter', $letterattachmentletter);
    }

    /**
     * Show the form for editing the letterattachmentletter.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $letterattachmentletter = $this->service->find($id);
        return view('letterattachmentletters.edit')->with('letterattachmentletter', $letterattachmentletter);
    }

    /**
     * Update the letterattachmentletters in storage.
     *
     * @param  App\Http\Requests\LetterattachmentletterUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LetterattachmentletterUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the letterattachmentletters from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('letterattachmentletters.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('letterattachmentletters.index'))->withErrors('Failed to delete');
    }
}
