<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\RecipientService;
use App\Http\Requests\RecipientCreateRequest;
use App\Http\Requests\RecipientUpdateRequest;

class RecipientsController extends Controller
{
    public function __construct(RecipientService $recipient_service)
    {
        $this->service = $recipient_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $recipients = $this->service->paginated();
        return view('recipients.index')->with('recipients', $recipients);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $recipients = $this->service->search($request->search);
        return view('recipients.index')->with('recipients', $recipients);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recipients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\RecipientCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RecipientCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('recipients.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('recipients.index'))->withErrors('Failed to create');
    }

    /**
     * Display the recipient.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $recipient = $this->service->find($id);
        return view('recipients.show')->with('recipient', $recipient);
    }

    /**
     * Show the form for editing the recipient.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $recipient = $this->service->find($id);
        return view('recipients.edit')->with('recipient', $recipient);
    }

    /**
     * Update the recipients in storage.
     *
     * @param  App\Http\Requests\RecipientUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RecipientUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the recipients from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('recipients.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('recipients.index'))->withErrors('Failed to delete');
    }
}
