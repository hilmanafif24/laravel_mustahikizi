<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LetterService;
use App\Http\Requests\LetterCreateRequest;
use App\Http\Requests\LetterUpdateRequest;

use App\Models\Letterattachment;
use App\Models\Letter;
use App\Models\Letterattachmentletter;
use App\Models\Recipient;
use Auth;

use Grafite\Cms\Services\FileService;
use Storage;
use Config;
use DB;

class LettersController extends Controller
{
    public function __construct(LetterService $letter_service)
    {
        $this->service = $letter_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $letters = $this->service->paginated();
        return view('letters.index')->with('letters', $letters);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $letters = $this->service->search($request->search);
        return view('letters.index')->with('letters', $letters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('letters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\LetterCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LetterCreateRequest $request)
    {
        $request->merge([
            'sender'=> Auth::user()->name,
            'sender_user_id' => Auth::user()->id
        ]);

        $result = $this->service->create($request->except('_token', 'lampiran_upload', 'nama_file'));

        $letter_o = DB::table('letters')->orderBy('id','DESC')->first();
        $letter = $letter_o->id;

        $recipient = new Recipient;
        $recipient->lt_letter_id =$letter;
        $recipient->user_id = $request->recipient;
        $recipient->save(); 

        if ($request->lampiran_upload) {
            $file = request()->file('lampiran_upload')->store('public/lampiran');
            //$path = app(FileService::class)->saveFile($file, 'public/lampiran', [], true); 
            $lampiran = new Letterattachment;

            $lampiran->name =  $request->name_file;
            $lampiran->path = $file;
            $lampiran->description = $request->name_file;
            $lampiran->user_id = Auth::user()->id;
            $lampiran->save(); 

            $lettera_o = DB::table('letterattachments')->orderBy('id','DESC')->first();
            $letter_at = $lettera_o->id;

            $lt = new Letterattachmentletter;
            $lt->lt_letter_id = $letter;
            $lt->lt_attachment_id = $letter_at;
            $lt->save(); 
        }
        

        if(Auth::user()->roles[0]->name == "pemberi_perintah"){
            if ($result) {
                return redirect('manajer/spk')->with('message', 'Successfully created');
            }
            return redirect('manajer/spk')->withErrors('Failed to create');

        }elseif (Auth::user()->roles[0]->name == "pemberi_tugas") {
            # code...
            if ($result) {
                return redirect('supervisor/surtu')->with('message', 'Successfully created');
            }
            return redirect('supervisor/surtu')->withErrors('Failed to create');
        }elseif(Auth::user()->roles[0]->name == "pelaksana"){
            if ($result) {
                return redirect('pelaksana/laporan')->with('message', 'Successfully created');
            }
            return redirect('pelaksana/laporan')->withErrors('Failed to create');
        }

       

        return redirect(route('letters.index'))->withErrors('Failed to create');
    }

    /**
     * Display the letter.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $letter = $this->service->find($id);
        return view('letters.show')->with('letter', $letter);
    }

    /**
     * Show the form for editing the letter.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $letter = $this->service->find($id);
        return view('letters.edit')->with('letter', $letter);
    }

    /**
     * Update the letters in storage.
     *
     * @param  App\Http\Requests\LetterUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LetterUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the letters from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('letters.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('letters.index'))->withErrors('Failed to delete');
    }
}
