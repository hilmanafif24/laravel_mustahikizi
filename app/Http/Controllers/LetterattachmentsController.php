<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LetterattachmentService;
use App\Http\Requests\LetterattachmentCreateRequest;
use App\Http\Requests\LetterattachmentUpdateRequest;

class LetterattachmentsController extends Controller
{
    public function __construct(LetterattachmentService $letterattachment_service)
    {
        $this->service = $letterattachment_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $letterattachments = $this->service->paginated();
        return view('letterattachments.index')->with('letterattachments', $letterattachments);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $letterattachments = $this->service->search($request->search);
        return view('letterattachments.index')->with('letterattachments', $letterattachments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('letterattachments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\LetterattachmentCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LetterattachmentCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('letterattachments.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('letterattachments.index'))->withErrors('Failed to create');
    }

    /**
     * Display the letterattachment.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $letterattachment = $this->service->find($id);
        return view('letterattachments.show')->with('letterattachment', $letterattachment);
    }

    /**
     * Show the form for editing the letterattachment.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $letterattachment = $this->service->find($id);
        return view('letterattachments.edit')->with('letterattachment', $letterattachment);
    }

    /**
     * Update the letterattachments in storage.
     *
     * @param  App\Http\Requests\LetterattachmentUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LetterattachmentUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the letterattachments from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('letterattachments.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('letterattachments.index'))->withErrors('Failed to delete');
    }
}
