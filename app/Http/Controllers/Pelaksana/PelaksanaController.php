<?php

namespace App\Http\Controllers\Pelaksana;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Letter;
use App\Models\Letterattachmentletter;
use App\Models\Letterattachment;


use Auth;
use Illuminate\Support\Facades\Schema;

class PelaksanaController extends Controller
{
    //
    public function index()
    {
    	 return redirect('/cms/dashboard');
    }

    public function surtu()
    {

        $user_id = Auth::user()->id;
        $letters = Letter::where('recipient', $user_id)->where('lt_type_id', 2)->orderBy('id', 'DESC')
        ->paginate(25);
        return view('pelaksana.surtu')->with('letters', $letters);
    }

    public function surtu_show($id)
    {
         $letter = Letter::find($id);
         $letter->has_read = 1;
         $letter->save();

         $attachment = Letterattachmentletter::where('lt_letter_id', $id)->first();

         if($attachment != null){
            $attachment_url = Letterattachment::find($attachment->lt_attachment_id);
        }else{
            $attachment_url = "not found";
        }

         return view('pelaksana.showsurtu')->with('letter', $letter)->with('attachment', $attachment_url);

    }

     public function laporan()
    {
        $user_id = Auth::user()->id;
        $letters = Letter::where('sender_user_id', $user_id)->where('lt_type_id', 3)->orderBy('id', 'DESC')
        ->paginate(25);
        return view('pelaksana.laporan')->with('letters', $letters);
      
    }

    public function laporan_show($id)
    {

        $letter = Letter::find($id);
        $attachment = Letterattachmentletter::where('lt_letter_id', $id)->first();

        if($attachment != null){
            $attachment_url = Letterattachment::find($attachment->lt_attachment_id);
        }else{
            $attachment_url = "not found";
        }

         return view('pelaksana.showlaporan')->with('letter', $letter)->with('attachment', $attachment_url);;
      
    }

    public function laporan_add($id)
    {
        $surtu = Letter::find($id);
        return view('pelaksana.laporancreate')->with('surtu', $surtu);
    }

    public function laporan_store()
    {


    }
}
