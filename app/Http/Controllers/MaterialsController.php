<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\MaterialService;
use App\Http\Requests\MaterialCreateRequest;
use App\Http\Requests\MaterialUpdateRequest;

class MaterialsController extends Controller
{
    public function __construct(MaterialService $material_service)
    {
        $this->service = $material_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $materials = $this->service->paginated();
        return view('materials.index')->with('materials', $materials);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $materials = $this->service->search($request->search);
        return view('materials.index')->with('materials', $materials);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('materials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\MaterialCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaterialCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('materials.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('materials.index'))->withErrors('Failed to create');
    }

    /**
     * Display the material.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $material = $this->service->find($id);
        return view('materials.show')->with('material', $material);
    }

    /**
     * Show the form for editing the material.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $material = $this->service->find($id);
        return view('materials.edit')->with('material', $material);
    }

    /**
     * Update the materials in storage.
     *
     * @param  App\Http\Requests\MaterialUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MaterialUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the materials from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('materials.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('materials.index'))->withErrors('Failed to delete');
    }
}
