<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\LettersourceService;
use App\Http\Requests\LettersourceCreateRequest;
use App\Http\Requests\LettersourceUpdateRequest;

class LettersourcesController extends Controller
{
    public function __construct(LettersourceService $lettersource_service)
    {
        $this->service = $lettersource_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lettersources = $this->service->paginated();
        return view('lettersources.index')->with('lettersources', $lettersources);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $lettersources = $this->service->search($request->search);
        return view('lettersources.index')->with('lettersources', $lettersources);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lettersources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\LettersourceCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LettersourceCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('lettersources.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('lettersources.index'))->withErrors('Failed to create');
    }

    /**
     * Display the lettersource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lettersource = $this->service->find($id);
        return view('lettersources.show')->with('lettersource', $lettersource);
    }

    /**
     * Show the form for editing the lettersource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lettersource = $this->service->find($id);
        return view('lettersources.edit')->with('lettersource', $lettersource);
    }

    /**
     * Update the lettersources in storage.
     *
     * @param  App\Http\Requests\LettersourceUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LettersourceUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the lettersources from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('lettersources.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('lettersources.index'))->withErrors('Failed to delete');
    }
}
