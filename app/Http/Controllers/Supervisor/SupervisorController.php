<?php

namespace App\Http\Controllers\Supervisor;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Letter;
use App\Models\Letterattachmentletter;
use App\Models\Letterattachment;


use Auth;
use Illuminate\Support\Facades\Schema;

class SupervisorController extends Controller
{
    //
    public function index()
    {
    	 return redirect('/cms/dashboard');
    }

     public function spk()
    {

        $user_id = Auth::user()->id;
        $letters = Letter::where('recipient', $user_id)->where('lt_type_id', 1)->orderBy('id', 'DESC')
        ->paginate(15);
        return view('supervisor.spk')->with('letters', $letters);
    }

    public function spk_show($id)
    {

    	// belum difilter id surat + id user yang menerima/mengirim 
         $letter = Letter::find($id);
          $letter->has_read = 1;
        $letter->save();
         $attachment = Letterattachmentletter::where('lt_letter_id', $id)->first();

         if($attachment != null){
            $attachment_url = Letterattachment::find($attachment->lt_attachment_id);
        }else{
            $attachment_url = "not found";
        }

         return view('supervisor.showspk')->with('letter', $letter)->with('attachment', $attachment_url);

    }

    public function surtu()
    {

        $user_id = Auth::user()->id;
        $letters = Letter::where('sender_user_id', $user_id)->where('lt_type_id', 2)->orderBy('id', 'DESC')
        ->paginate(25);
        return view('supervisor.surtu')->with('letters', $letters);
    }

    public function surtu_show($id)
    {
         $letter = Letter::find($id);

           $attachment = Letterattachmentletter::where('lt_letter_id', $id)->first();

         if($attachment != null){
            $attachment_url = Letterattachment::find($attachment->lt_attachment_id);
        }else{
            $attachment_url = "not found";
        }

         return view('supervisor.showsurtu')->with('letter', $letter)->with('attachment', $attachment_url);

    }

    public function surtu_add($id)
    {
    	// query dulu spk referensi
        $spk = Letter::find($id);
        return view('supervisor.surtucreate')->with('spk', $spk);

    }

    public function surtu_store()
    {


    }


    public function laporan()
    {
        $user_id = Auth::user()->id;
        $letters = Letter::where('sender_user_id', $user_id)->where('lt_type_id', 3)->orderBy('id', 'DESC')
        ->paginate(25);
        return view('supervisor.laporan')->with('letters', $letters);
      
    }

    public function laporan_show($id)
    {
        $letter = Letter::find($id);
        $attachment = Letterattachmentletter::where('lt_letter_id', $id)->first();

        if($attachment != null){
            $attachment_url = Letterattachment::find($attachment->lt_attachment_id);
        }else{
            $attachment_url = "not found";
        }

         return view('supervisor.showlaporan')->with('letter', $letter)->with('attachment', $attachment_url);
      
    }

    public function laporan_add($id)
    {
        // query dulu spk referensi
        $spk = Letter::find($id);
        return view('supervisor.laporancreate')->with('spk', $spk);
    }

    public function laporan_store()
    {


    }

    public function laporanpelaksana()
    {
        $user_id = Auth::user()->id;
        $letters = Letter::where('recipient', $user_id)->where('lt_type_id', 3)
        ->paginate(25);
        return view('supervisor.laporanpelaksana')->with('letters', $letters);
      
    }

    public function laporanpelaksana_show($id)
    {
        $letter = Letter::find($id);
        $letter->has_read = 1;
        $letter->save();
        $attachment = Letterattachmentletter::where('lt_letter_id', $id)->first();

        if($attachment != null){
            $attachment_url = Letterattachment::find($attachment->lt_attachment_id);
        }else{
            $attachment_url = "not found";
        }

         return view('supervisor.showlaporanpelaksana')->with('letter', $letter)>with('attachment', $attachment_url);
      
    }
}
