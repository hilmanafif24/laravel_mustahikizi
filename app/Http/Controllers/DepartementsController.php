<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\DepartementService;
use App\Http\Requests\DepartementCreateRequest;
use App\Http\Requests\DepartementUpdateRequest;

class DepartementsController extends Controller
{
    public function __construct(DepartementService $departement_service)
    {
        $this->service = $departement_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $departements = $this->service->paginated();
        return view('departements.index')->with('departements', $departements);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $departements = $this->service->search($request->search);
        return view('departements.index')->with('departements', $departements);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\DepartementCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepartementCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('departements.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('departements.index'))->withErrors('Failed to create');
    }

    /**
     * Display the departement.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $departement = $this->service->find($id);
        return view('departements.show')->with('departement', $departement);
    }

    /**
     * Show the form for editing the departement.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departement = $this->service->find($id);
        return view('departements.edit')->with('departement', $departement);
    }

    /**
     * Update the departements in storage.
     *
     * @param  App\Http\Requests\DepartementUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DepartementUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the departements from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('departements.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('departements.index'))->withErrors('Failed to delete');
    }
}
