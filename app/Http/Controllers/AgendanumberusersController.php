<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\AgendanumberuserService;
use App\Http\Requests\AgendanumberuserCreateRequest;
use App\Http\Requests\AgendanumberuserUpdateRequest;

class AgendanumberusersController extends Controller
{
    public function __construct(AgendanumberuserService $agendanumberuser_service)
    {
        $this->service = $agendanumberuser_service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $agendanumberusers = $this->service->paginated();
        return view('agendanumberusers.index')->with('agendanumberusers', $agendanumberusers);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $agendanumberusers = $this->service->search($request->search);
        return view('agendanumberusers.index')->with('agendanumberusers', $agendanumberusers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agendanumberusers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\AgendanumberuserCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AgendanumberuserCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('agendanumberusers.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('agendanumberusers.index'))->withErrors('Failed to create');
    }

    /**
     * Display the agendanumberuser.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agendanumberuser = $this->service->find($id);
        return view('agendanumberusers.show')->with('agendanumberuser', $agendanumberuser);
    }

    /**
     * Show the form for editing the agendanumberuser.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agendanumberuser = $this->service->find($id);
        return view('agendanumberusers.edit')->with('agendanumberuser', $agendanumberuser);
    }

    /**
     * Update the agendanumberusers in storage.
     *
     * @param  App\Http\Requests\AgendanumberuserUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AgendanumberuserUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            return back()->with('message', 'Successfully updated');
        }

        return back()->withErrors('Failed to update');
    }

    /**
     * Remove the agendanumberusers from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('agendanumberusers.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('agendanumberusers.index'))->withErrors('Failed to delete');
    }
}
