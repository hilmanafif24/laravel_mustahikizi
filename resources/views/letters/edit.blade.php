@extends('cms::layouts.dashboard', ['pageTitle' => 'Letters &raquo; Edit'])

@section('content')

    <div class="row raw-margin-top-24 raw-margin-left-24 raw-margin-right-24">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'letters.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Letters: Edit</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('letters.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row raw-margin-top-24 raw-margin-left-24 raw-margin-right-24">
        <div class="col-md-12">

            {!! Form::model($letter, ['route' => ['letters.update', $letter->id], 'method' => 'patch', 'files'=>true]) !!}

             <div class="row">
                <div class="raw-margin-top-24 col-md-3">
                    @input_maker_label('Sumber Surat')
                    @input_maker_create('lt_source_id', ['type' => 'integer', 'placeholder' => 'Sumber Surat'], $letter)
                </div>

                <div class="raw-margin-top-24 col-md-6">
                    @input_maker_label('Nomor Sumber Surat')
                    @input_maker_create('agenda_no', ['type' => 'string', 'placeholder' => 'No Sumber Surat'], $letter)
                </div>  
            </div>

            <div class="row">
                <div class="raw-margin-top-24 col-md-3">
                    @input_maker_label('Jenis Surat')
                    @input_maker_create('lt_type_id', ['type' => 'integer', 'placeholder' => 'Jenis Surat'], $letter)
                 </div>

                <div class="raw-margin-top-24 col-md-6">
                    @input_maker_label('Nomor Surat')
                    @input_maker_create('letter_no', ['type' => 'string', 'placeholder' => 'Nomor Surat'], $letter)
                </div>
                
            </div>


            <div class="raw-margin-top-24">
                @input_maker_label('Kepada')
                @input_maker_create('recipient', ['type' => 'string', 'placeholder' => 'Kepada'], $letter)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Perihal')
                @input_maker_create('subject', ['type' => 'string', 'placeholder' => 'Perihal'], $letter)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Isi')
                @input_maker_create('body', ['type' => 'text', 'class'=>'redactor', 'placeholder' => 'Isi'], $letter)
            </div>
           
            <div class="raw-margin-top-24">
                @input_maker_label('Nomor Anggaran')
                @input_maker_create('lt_money', ['type' => 'string', 'placeholder' => 'Nomor Anggaran'], $letter)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Alat dan Barang')
                @input_maker_create('lt_material', ['type' => 'text', 'class'=>'redactor',  'placeholder' => 'Alat dan Barang'], $letter)
            </div>

             <div class="raw-margin-top-24">
                @input_maker_label('Lampiran')
                @input_maker_create('lampiran_upload', ['type'=>'file'])
            </div>    

            <div class="raw-margin-top-24 raw-margin-left-24">
                {!! Form::submit('Update', ['class' => 'btn btn-primary pull-right']) !!}
                
            </div>

            

            {!! Form::close() !!}

        </div>
    </div>

@stop
