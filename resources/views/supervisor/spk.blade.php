@extends('cms::layouts.dashboard', ['pageTitle' => 'Letters &raquo; Index'])

@section('content')

    <!-- <nav aria-label="breadcrumb" >
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Library</a></li>
        <li class="breadcrumb-item active" aria-current="page">Data</li>
      </ol>
    </nav> -->

    <div class="row raw-margin-top-24 raw-margin-left-24 raw-margin-right-24"">
        <div class="col-md-12">
            <!-- <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'letters.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div> -->
            <h2 class="pull-left"><span class="fa fa-envelope"> </span>Surat Perintah Kerja</h2>
            <!-- <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('letters.create') !!}">++ Kirim Surat</a> -->
        </div>
    </div>

    <div class="row raw-margin-top-24 raw-margin-left-24 raw-margin-right-24"">
        <div class="col-md-12">
            @if ($letters->isEmpty())
                <div class="well text-center">Surat Perintah Kerja Tidak ditemukan.</div>
            @else
                <table class="table table-striped">
                    <thead>
                        <th>Tgl</th>
                        <th>Pengirim</th>
                        <th>No. Surat</th>
                        <th>Perihal</th>
                        <th class="text-right" width="450px">Action</th>
                    </thead>
                    <tbody>
                        @foreach($letters as $letter)
                            <tr>
                                <td>
                                    <a>{{ date('d/m/Y', strtotime($letter->created_at)) }}</a>
                                </td>
                                <td>
                                    <a>{{ $letter->sender }}</a>
                                </td>
                                <td>
                                    <a>{{ $letter->letter_no }}</a>
                                </td>
                                <td>
                                    <a href="{{ url('supervisor/spk/'.$letter->id) }}">{{ $letter->subject }}</a>
                                </td>
                               
                                <td class="text-right">
                                    
                                    <!-- <form method="post" action="{!! route('letters.destroy', [$letter->id]) !!}">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}
                                        <button class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this letter?')"><i class="fa fa-trash"></i> Delete</button>
                                    </form> -->
                                    <a class="btn btn-info btn-xs pull-right raw-margin-right-16" href="{{url('supervisor/createsurtu/'.$letter->id)}}"><i class="fa fa-pencil"></i> Buat Surat Tugas</a>
                                    <a class="btn btn-warning btn-xs pull-right raw-margin-right-16" href="{{url('supervisor/createlaporan/'.$letter->id)}}"><i class="fa fa-pencil"></i> Buat Laporan SPK</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {!! $letters; !!}
        </div>
    </div>

@stop