@extends('cms-frontend::layout.master')

@section('content')

    <div class="form-small">

        <h2 class="text-center">Please sign in</h2>

        <form method="POST" action="/otp">
            {!! csrf_field() !!}
            <div class="col-md-12 mt-3">
                
                <label>Handphone</label>
                    <div class="row">
                        <div class="col-sm-1">
                            <input class="form-control" placeholder="+62" readonly="true">
                        </div>
                        <div class="col-sm">
                            <input class="form-control" type="text" name="handphone" placeholder="Handphone" value="{{old('handphone')}}">
                        </div>
                    </div>
                    
                <div class="row">
                    <label>Phone (Testing): 83100180252</label>
                </div>
                <div class="row">
                    <label>Phone (Testing): 895352394642</label>
                </div>
                
            </div>
            <div class="col-md-12 mt-3">
                <div class="btn-toolbar justify-content-between">
                    <button class="btn btn-primary" type="submit">Login</button>
                  
                </div>
            </div>

            @if (config('cms.registration-available'))
                <div class="col-md-12 mt-3">
                    <a class="btn raw100 btn-info btn-block" href="/register">Register</a>
                </div>
            @endif
        </form>

    </div>

@stop

