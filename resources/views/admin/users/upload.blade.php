@extends('cms::layouts.dashboard')

@section('pageTitle') Users @stop

@section('content')

     <div class="col-md-12">
        <nav class="navbar px-0 navbar-light justify-content-between">
        	<h3 class="pull-left"><span class="fa fa-users"></span> Import User By Handphone Number </h3>

        </nav>

    
    </div>

	<div class="alert alert-warning alert-dismissible" role="alert">
	  <strong>Peringatan!</strong> Anda akan menambah user dengan Import Excel. Pastikan Kolom <b>Handphone</b> tersedia pada excel anda.
	  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	    <span aria-hidden="true">&times;</span>
	  </button>
	</div>
	


     <div class="col-md-12">


			{!! Form::open(['url' => 'admin/users/import', 'method' => 'post', 'class' => 'form-inline mt-2', 'enctype' => 'multipart/form-data']) !!}
			{!! csrf_field() !!}
                {!! method_field('POST') !!}
               
                <input class="form-control mr-sm-2" name="file" type="file" placeholder="Upload Excel" aria-label="Upload Excel">
               
                  
                @input_maker_create('company_id', ['type' => 'relationship', 'model' => 'App\Models\Company', 'label' => 'name', 'value' => 'id'])
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Upload</button>
               
            {!! Form::close() !!}
      
            <div class="card card-dark text-center mt-4">
               
            </div>

            @if (empty($clients))
            <div class="card card-dark text-center mt-4">
               
                <div class="card-body">No users found.</div>
            </div>
       		@else
       		<table class="table table-striped">
                <thead>
                    <th>No.Handphone</th>
                   
                </thead>
                <tbody>
                    @foreach($clients as $user)
                        <tr>
                            <td>{{ $user->handphone }}</td>  
                        </tr>
                    @endforeach
                </tbody>
            </table>
       		@endif
      
    </div>

@stop
