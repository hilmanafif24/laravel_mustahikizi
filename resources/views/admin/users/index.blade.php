@extends('cms::layouts.dashboard')

@section('pageTitle') Users @stop

@section('content')

    <div class="col-md-12">
        <nav class="navbar px-0 navbar-light justify-content-between">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="btn btn-success" href="{{ url('admin/users/invite') }}">Invite New User</a>
                    <a class="btn btn-primary" href="{!! url('admin/users/create') !!}">Add New User</a>
                    <a class="btn btn-warning" href="{!! url('admin/users/upload') !!}">Upload Excel</a>
                </li>
            </ul>

            {!! Form::open(['url' => 'admin/users/search', 'method' => 'post', 'class' => 'form-inline mt-2']) !!}
                <input class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            {!! Form::close() !!}

             <form id="" class="pull-right raw-margin-top-14 raw-margin-left-24 form-inline" method="post" action="/admin/users/searchcompany">
              {!! csrf_field() !!}
              <!-- <input class="form-control form-inline pull-right" name="search" placeholder="Search"> -->
               <!-- <label for="sel1">Filter Company:</label> -->
                <select class="form-control" name="company" id="company">
                    <option value="">Filter By Company</option>
                    @foreach($companies as $company)
                        <option value="{{$company->id}}">{{$company->name}}</option>
                    @endforeach
                </select>
              <button class="btn btn-primary pull-right" style="margin-left: 10px" type="submit">Filter</button>
              
            {!! Form::close() !!}


            
            
        </nav>
    </div>

    <div class="col-md-12">
        @if(isset($perusahaan))
            <h3 class="pull-left"><span class="fa fa-users"></span> Kelola User {{$perusahaan->name}}</h1>
        @else
            <h3 class="pull-left"><span class="fa fa-users"></span> Kelola User</h1>
        @endif

        @if ($users->count() === 0)
            <div class="card card-dark text-center mt-4">
                @if (request('search'))
                    <div class="card-header">Searched for "{{ request('search') }}"</div>
                @endif
                <div class="card-body">No users found.</div>
            </div>
        @else
       
            <table class="table table-striped">
                <thead>
                    <th>No.Handphone</th>
                    <th>Email</th>
                    <th width="170px" class="text-right">Actions</th>
                </thead>
                <tbody>
                    @foreach($users as $user)
                        @if ($user->id !== Auth::id())
                            <tr>
                                <td>{{ $user->handphone }}</td>
                                <td>{{ $user->email }}</td>
                                <td class="text-right">
                                    <div class="btn-toolbar justify-content-between">
                                        <a class="btn btn-outline-primary btn-sm mr-2" href="{{ url('admin/users/'.$user->id.'/edit') }}"><span class="fa fa-edit"></span> Edit</a>
                                        <form method="post" action="{!! url('admin/users/'.$user->id) !!}">
                                            {!! csrf_field() !!}
                                            {!! method_field('DELETE') !!}
                                            <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('Are you sure you want to delete this user?')"><i class="fa fa-trash"></i> Delete</button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        @endif
    </div>

@stop
