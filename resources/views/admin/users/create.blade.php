@extends('cms::layouts.dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
     
          <h2 class="pull-left"><span class="fa fa-users"></span> Tambah User</h2>
        </div>
    </div>

    <!-- <div class="row raw-margin-top-24"> -->
        <div class="col-md-12">
            <form method="POST" action="/admin/users/store/">

                {!! csrf_field() !!}
                {!! method_field('POST') !!}


                <div class="raw-margin-top-24">
                    @input_maker_label('Email')
                    @input_maker_create('email', ['type' => 'string'])
                </div>

                <div class="raw-margin-top-24">
                    @input_maker_label('Name')
                    @input_maker_create('name', ['type' => 'string'])
                </div>

                <div class="raw-margin-top-24">
                    @input_maker_label('Handphone')
                    @input_maker_create('handphone', ['type' => 'string'])
                </div>
                
                <div class="raw-margin-top-24">
                    @input_maker_label('Role')
                    @input_maker_create('roles', ['type' => 'relationship', 'model' => 'App\Models\Role', 'label' => 'label', 'value' => 'name'])
                </div>

                <div class="raw-margin-top-24">
                    @input_maker_label('Company')
                     
                    @input_maker_create('company_id', ['type' => 'relationship', 'model' => 'App\Models\Company', 'label' => 'name', 'value' => 'id'])
                </div>

                <div class="raw-margin-top-24">
                    <a class="btn btn-warning pull-left" href="{{ URL::previous() }}">Cancel</a>
                    <button class="btn btn-primary pull-right" type="submit">Save</button>
                </div>
            {!! Form::close() !!}
        </div>
    <!-- </div> -->

@stop
