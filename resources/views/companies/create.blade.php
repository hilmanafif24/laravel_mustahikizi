@extends('cms::layouts.dashboard', ['pageTitle' => 'Companies &raquo; Create'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'companies.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Companies: Create</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            {!! Form::open(['route' => 'companies.store', 'files' => true]) !!}

            <div class="raw-margin-top-24">
                @input_maker_label('Name')
                @input_maker_create('name', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Tax')
                @input_maker_create('tax', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Reg')
                @input_maker_create('reg', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Phone')
                @input_maker_create('phone', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Fax')
                @input_maker_create('fax', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Address1')
                @input_maker_create('address1', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Address2')
                @input_maker_create('address2', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('City')
                @input_maker_create('city', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Province')
                @input_maker_create('province', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('ZIP')
                @input_maker_create('zip', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Country')
                @input_maker_create('country', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Logo')
                @input_maker_create('logo2', ['type' => 'file'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Timezone')
                @input_maker_create('timezone', ['type' => 'string'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Currency')
                @input_maker_create('currency', ['type' => 'string'])
            </div>

            {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

          

        </div>
    </div>

@stop