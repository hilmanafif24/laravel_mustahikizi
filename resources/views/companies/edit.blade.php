@extends('cms::layouts.dashboard', ['pageTitle' => 'Companies &raquo; Edit'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'companies.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Companies: Edit</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('companies.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
             {!! Form::model($company, ['route' => ['companies.update', $company->id], 'method' => 'patch', 'files' => true]) !!}

            <div class="raw-margin-top-24">
                @input_maker_label('Name')
                @input_maker_create('name', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Tax')
                @input_maker_create('tax', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Reg')
                @input_maker_create('reg', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Phone')
                @input_maker_create('phone', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Fax')
                @input_maker_create('fax', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Address1')
                @input_maker_create('address1', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Address2')
                @input_maker_create('address2', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('City')
                @input_maker_create('city', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Province')
                @input_maker_create('province', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('ZIP')
                @input_maker_create('zip', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Country')
                @input_maker_create('country', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                <img class="img-thumbnail img-fluid" src="{{ $image}}" alt="" style="height: 160px; width:160px;">
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Logo')
                @input_maker_create('logo2', ['type' => 'file'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Timezone')
                @input_maker_create('timezone', ['type' => 'string'], $company)
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Currency')
                @input_maker_create('currency', ['type' => 'string'], $company)
            </div>

            {!! Form::submit('Save', ['class' => 'btn btn-primary pull-right']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@stop
