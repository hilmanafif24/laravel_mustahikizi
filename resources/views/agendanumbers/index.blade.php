@extends('cms::layouts.dashboard', ['pageTitle' => 'Agendanumbers &raquo; Index'])

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'agendanumbers.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div>
            <h1 class="pull-left">Agendanumbers</h1>
            <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('agendanumbers.create') !!}">Add New</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @if ($agendanumbers->isEmpty())
                <div class="well text-center">No agendanumbers found.</div>
            @else
                <table class="table table-striped">
                    <thead>
                        <th>Name</th>
                        <th class="text-right" width="200px">Action</th>
                    </thead>
                    <tbody>
                        @foreach($agendanumbers as $agendanumber)
                            <tr>
                                <td>
                                    <a href="{!! route('agendanumbers.edit', [$agendanumber->id]) !!}">{{ $agendanumber->name }}</a>
                                </td>
                                <td class="text-right">
                                    <form method="post" action="{!! route('agendanumbers.destroy', [$agendanumber->id]) !!}">
                                        {!! csrf_field() !!}
                                        {!! method_field('DELETE') !!}
                                        <button class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this agendanumber?')"><i class="fa fa-trash"></i> Delete</button>
                                    </form>
                                    <a class="btn btn-default btn-xs pull-right raw-margin-right-16" href="{!! route('agendanumbers.edit', [$agendanumber->id]) !!}"><i class="fa fa-pencil"></i> Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 text-center">
            {!! $agendanumbers; !!}
        </div>
    </div>

@stop