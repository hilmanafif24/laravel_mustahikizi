@extends('cms::layouts.dashboard')

@section('pageTitle') Dashboard @stop

@section('content')

    <div class="col-md-12">
   
        <br />

        <div class="row">
            <div class="col-md-5">
              <div class="jumbotron" style="background:#18bc9c; color:#ffffff; height:290px; padding:20px;">
                  <h2>Selamat Datang</h2>
                  
                  <p> Anda Login Sebagai : <b>{{ Auth::user()->name }}</b>. </p>
                  <p> Inisiatif Zakat Indonesia</b>. </p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="jumbotron" style="height:290px; padding:20px;">
                  <!-- <canvas id="canvas" height="280" width="600"></canvas> -->
                   <canvas id="dashboardChart" class="raw100"></canvas>
              </div>
            </div>
        </div>

        <div class="row">
            @if(Auth::user()->roles[0]->name == "pemberi_perintah")
            <div class="col-lg-2">
                <div class="card card-dark text-center">
                    <div class="card-header">
                        Jumlah Mustahik
                    </div>
                    <div class="card-body">
                        12
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="card card-dark text-center">
                    <div class="card-header">
                        Jumlah Pemohon
                    </div>
                    <div class="card-body">
                        20
                    </div>
                </div>
            </div>
            @endif
            
            @if(Auth::user()->roles[0]->name == "pemberi_tugas" )
            <div class="col-lg-2">
                <div class="card card-dark text-center">
                    <div class="card-header">
                        Jumlah Mustahik
                    </div>
                    <div class="card-body">
                        10
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="card card-dark text-center">
                    <div class="card-header">
                        Jumlah Pemohon
                    </div>
                    <div class="card-body">
                        3
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="card card-dark text-center">
                    <div class="card-header">
                        Jumlah Pemohon
                    </div>
                    <div class="card-body">
                        14
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="card card-dark text-center">
                    <div class="card-header">
                        Jumlah Pencairan
                    </div>
                    <div class="card-body">
                        9
                    </div>
                </div>
            </div>
            @endif

            @if(Auth::user()->roles[0]->name == "pelaksana")
            <div class="col-lg-2">
                <div class="card card-dark text-center">
                    <div class="card-header">
                        Jumlah Pemohon
                    </div>
                    <div class="card-body">
                        19
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="card card-dark text-center">
                    <div class="card-header">
                        Jumlah Laporan
                    </div>
                    <div class="card-body">
                        15
                    </div>
                </div>
            </div>
            @endif
         
            
           
        </div>
    </div>


@stop

@section('javascript')
    @parent
    <script type="text/javascript">
        var _chartData = {
            _labels : {!! json_encode($stats['dates']) !!},
            _visits : {!! json_encode($stats['visits']) !!}
        };
        var options = {};
    </script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js"></script>
    {!! Minify::javascript(Cms::asset('js/dashboard-chart.js')) !!}
  
@stop