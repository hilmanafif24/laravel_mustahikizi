@extends('cms::layouts.dashboard', ['pageTitle' => 'Laporan &raquo; Show'])

@section('content')

    <div class="row raw-margin-top-24 raw-margin-left-24 raw-margin-right-24">
        <div class="col-md-12">
            <!-- <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'letters.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div> -->
            <h1 class="pull-left">Laporan</h1>
            <!-- <a class="btn btn-primary pull-right raw-margin-top-24 raw-margin-right-8" href="{!! route('letters.create') !!}">Add New</a> -->
        </div>
    </div>

    <div class="row raw-margin-top-24 raw-margin-left-24 raw-margin-right-24">
        <div class="col-md-12">

           <div class="row">
                <div class="raw-margin-top-24 col-md-3">
                    <b>@input_maker_label('Sumber Surat')</b>

                    <div class="panel panel-default">
                      <div class="panel-body">
                        {{ $letter->lt_source_id > 0 ? App\Models\Lettersource::find($letter->lt_source_id)->name : '-' }}
                      </div>
                    </div>
                    
                </div>

                <div class="raw-margin-top-24 col-md-6">
                    <b>@input_maker_label('Nomor Sumber Surat (No. Tiket, No Disposisi, No Proker)')</b>
                   
                    <div class="panel panel-default">
                      <div class="panel-body">
                        {{ $letter->agenda_no }}
                      </div>
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="raw-margin-top-24 col-md-3">
                    <b>@input_maker_label('Jenis Surat')</b>
                    
                    <div class="panel panel-default">
                      <div class="panel-body">
                        {{ $letter->lt_type_id > 0 ? App\Models\Lettertype::find($letter->lt_type_id)->name : '-' }}
                      </div>
                    </div>
                 </div>

                <div class="raw-margin-top-24 col-md-6">
                    <b>@input_maker_label('Nomor Surat')</b>
                   <div class="panel panel-default">
                      <div class="panel-body">
                        {{ $letter->letter_no }}
                      </div>
                    </div>
                </div>
                
            </div>


            <div class="raw-margin-top-24">
                <b>@input_maker_label('Kepada')</b>
                <div class="panel panel-default">
                      <div class="panel-body">
                        {{ $letter->recipient > 0 ? App\Models\User::find($letter->recipient)->name : '-' }}
                      </div>
                </div>
            </div>

            <div class="raw-margin-top-24">
                <b>@input_maker_label('Perihal')</b>
                <div class="panel panel-default">
                      <div class="panel-body">
                        {{ $letter->subject }}
                      </div>
                </div>
            </div>

            <div class="raw-margin-top-24">
                <b>@input_maker_label('Isi')</b>
               
                <div class="jumbotron">
                 
                  <p>{!! $letter->body !!}</p>
                  
                </div>
            </div>
           
            <!-- <div class="raw-margin-top-24">
                <b>@input_maker_label('Nomor Anggaran')</b>
                <div class="panel panel-default">
                      <div class="panel-body">
                        {{ $letter->lt_money }}
                      </div>
                </div>
            </div>

            <div class="raw-margin-top-24">
                <b>@input_maker_label('Alat dan Barang')</b>
               
                <div class="jumbotron">
                 
                  <p>{!! $letter->lt_material !!}</p>
                  
                </div>
            </div> -->

             <div class="raw-margin-top-24">
                <b>@input_maker_label('Lampiran')</b>
                <div class="panel panel-default">
                      <div class="panel-body">
                        @if($attachment != 'not found')
                          <a class="btn btn-info btn-xs raw-margin-right-16" href="{{ $attachment->getFileUrlAttribute($attachment) }}"><i class="fa fa-download"> </i> {!! $attachment->name !!}</a> 
                        @else
                          Tidak ada Lampiran
                        @endif
                      </div>
                </div>
            </div>    

        </div>
    </div>

@stop
