@extends('cms::layouts.dashboard', ['pageTitle' => 'Surat Perintah Kerja &raquo; Create'])

@section('content')

    <div class="row raw-margin-top-24 raw-margin-left-24 raw-margin-right-24">
        <div class="col-md-12">
           <!--  <div class="pull-right raw-margin-top-24 raw-margin-left-24">
                {!! Form::open(['route' => 'letters.search']) !!}
                <input class="form-control form-inline pull-right" name="search" placeholder="Search">
                {!! Form::close() !!}
            </div> -->
            <h1 class="pull-left">Surat Perintah Kerja</h1>
        </div>
    </div>

    <div class="row raw-margin-top-24 raw-margin-left-24 raw-margin-right-24">
        <div class="col-md-12">

            {!! Form::open(['route' => 'letters.store', 'files'=>true]) !!}

            <div class="row">
                <div class="raw-margin-top-24 col-md-3">
                    @input_maker_label('Sumber Surat')

                    @input_maker_create('lt_source_id', ['type' => 'relationship', 'model' => 'App\Models\Lettersource', 'label' => 'name', 'value' => 'id'])
                </div>

                <div class="raw-margin-top-24 col-md-6">
                    @input_maker_label('Nomor Sumber Surat (No. Tiket, No Disposisi, No Proker)')
                    @input_maker_create('agenda_no', ['type' => 'string', 'placeholder' => 'No Sumber Surat'])
                </div>  
            </div>

            <div class="row">
                <div class="raw-margin-top-24 col-md-3">
                    @input_maker_label('Jenis Surat')
                    @input_maker_create('lt_type_id', ['type' => 'select', 'alt_name' => 'Your Job','options' => [
                    'Surat Perintah Kerja' => '1',]])

                 </div>

                <div class="raw-margin-top-24 col-md-6">
                    @input_maker_label('Nomor Surat')
                    @input_maker_create('letter_no', ['type' => 'string', 'placeholder' => 'Nomor Surat'])
                </div>
            </div>


            <div class="raw-margin-top-24">
                @input_maker_label('Kepada')
                @input_maker_create('recipient', ['type' => 'relationship', 'model' => 'App\Models\User', 'label' => 'name', 'value' => 'id', 'method' => 'getChild', 'params'=>Auth::user()->id,'options' => [
                        'Pilih Pegawai' => '',
                    ]])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Perihal')
                @input_maker_create('subject', ['type' => 'string', 'placeholder' => 'Perihal'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Isi')
                @input_maker_create('body', ['type' => 'text', 'class'=>'redactor', 'placeholder' => 'Isi'])
            </div>
           
            <div class="raw-margin-top-24">
                @input_maker_label('Nomor Anggaran')
                @input_maker_create('lt_money', ['type' => 'string', 'placeholder' => 'Nomor Anggaran'])
            </div>

            <div class="raw-margin-top-24">
                @input_maker_label('Alat dan Barang')
                @input_maker_create('lt_material', ['type' => 'text', 'class'=>'redactor',  'placeholder' => 'Alat dan Barang'])
            </div>

             <div class="raw-margin-top-24">
                @input_maker_label('Lampiran')
                
               
            </div> 

            <div class="row">
                <div class="raw-margin-top-12 col-md-3">
                    @input_maker_create('name_file', ['type' => 'string', 'placeholder' => 'Nama File'])
                </div>

                <div class="raw-margin-top-12 col-md-6">
                     @input_maker_create('lampiran_upload', ['type'=>'file'])
                </div>  
            </div>


            <div class="raw-margin-top-24 raw-margin-left-24">
                {!! Form::submit('Kirim', ['class' => 'btn btn-primary pull-right']) !!}
                
            </div>

            {!! Form::close() !!}

        </div>
    </div>

@stop