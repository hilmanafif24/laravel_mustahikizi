<!doctype html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
        <title>{{ config('app.name') }} @if (isset($page) && !is_null($page->title)) - {{ $page->title }} @endif</title>
        <meta name="description" content="@yield('seoDescription')">
        <meta name="keywords" content="@yield('seoKeywords')">
        <meta name="author" content="">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/theme.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
        @yield('stylesheets')
    </head>

    <body id="page-top">

        @if (Route::getCurrentRoute()->uri() != '/')
            @theme('partials.navigation')
        @else
            <!-- Halaman home menggunakan navigation bar sendiri -->
            @yield('navbarhome')    
        @endif

        @yield('content')

    </body>

    <script type="text/javascript">
        var _token = '{!! csrf_token() !!}';
        var _url = '{!! url("/") !!}';
    </script>
    <script src="{{ url('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/popper.min.js') }}"></script>
    @yield("pre-javascript")
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
    @yield('javascript')
</html>
