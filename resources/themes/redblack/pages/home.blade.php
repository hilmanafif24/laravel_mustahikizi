<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ env('APP_NAME') }}</title>
    <link rel="shortcut icon" href="{{url('img/logomin.png')}}">
    <link rel="stylesheet" href="{{ url('css1/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('css1/magnific-popup.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('css1/creative.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('css1/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic" type="text/css">
</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">{{ env('APP_NAME') }}</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <!-- <li>
                        <a class="page-scroll" href="{!! url('pdashboard') !!}">Dashboard Publik</a>
                    </li> -->
                    <li>
                        <a class="page-scroll" href="#about">Tentang Aplikasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Fitur</a>
                    </li>
                    <li>
                      @if (Auth::user())
                      <a class="page-scroll" href="{!! url('dashboard') !!}">Control Panel</a>
                      @else
                      <a class="page-scroll" href="#page-top" data-toggle="modal" data-target="#myModal">Login</a>
                      @endif
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Kontak</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">Aplikasi Pengelolaan Mustahik IZI</h1>
                <hr>
                <p>Aplikasi ini dibuat untuk monitoring dan controlling permohonan pencairan zakat kepada mustahik </p>
                <a class="btn btn-success btn-xl page-scroll" href="#page-top" data-toggle="modal" data-target="#myModal">Silahkan Login</a>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Digitalisasi Distribusi Zakat</h2>
                    <hr class="light" style="border:1px solid;">
                    <p class="text-faded">Aplikasi ini merupakan kelanjutan dari Project E-Office, mengintegrasikan beberapa data diantara lain : data Surat dari E-Office dan Nomor tiker dari CRM</p>
                    <a href="#services" class="page-scroll btn btn-default btn-xl sr-button">Fitur</a>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Fitur &amp; Teknologi</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-institution text-primary sr-icons"></i>
                        <h3>Solid Stack</h3>
                        <p class="text-muted">Menggunakan web frameworks & teknologi yang update.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-cube text-primary sr-icons"></i>
                        <h3>Partnership Terpercaya</h3>
                        <p class="text-muted">Instalasi, pendampingan dan pengembangan</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-tachometer text-primary sr-icons"></i>
                        <h3>Mudah Digunakan</h3>
                        <p class="text-muted">Sederhana, dengan UI/UX yang slim dan bersih.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-mobile-phone text-primary sr-icons"></i>
                        <h3>Mobile</h3>
                        <p class="text-muted">Disertai aplikasi Android pelengkap.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter popup-gallery">
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/thumbnails/1.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/1.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    DOKUMEN, DIGITAL, MANAJERIAL   
                                </div>
                                <div class="project-name">
                                    Surat Perintah Kerja 
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="himg/portfolio/thumbnails/2.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/2.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                Dokumen, Digital, Pelaksana
                              </div>
                              <div class="project-name">
                                Surat Tugas
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/thumbnails/3.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/3.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                Review Aplikasi
                                  </div>
                              <div class="project-name">
                                Dashboard
                                  </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/thumbnails/4.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/4.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                Output Surat Perintah Kerja
                                  </div>
                              <div class="project-name">
                                Laporan Perintah Kerja
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/thumbnails/5.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/5.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                  Output Surat Tugas
                              </div>
                              <div class="project-name">
                                Laporan Pelaksana
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="img/portfolio/thumbnails/6.jpg" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/6.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                              <div class="project-category text-faded">
                                Output Surat Tugas
                                  </div>
                              <div class="project-name">
                                Berita Acara
                              </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Login terlebih dahulu untuk masuk ke sistem.</h2>
                <a href="#page-top" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-xl sr-button">Login</a>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Kontak Kami</h2>
                    <hr class="primary">
                    <p>Untuk informasi teknis atau inquiry, bisa menghubungi Pengembang &amp; Stakeholder Aplikasi:<br />
                      IZI Bandung<br />
                      PT Jerbee Indonesia, Bandung, Indonesia</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>+62 022 7323768</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p>info@jerbeeindonesia.com</a></p>
                </div>
            </div>
        </div>
    </section>

    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
            <div class="row">
        <form method="POST" action="{!! url('login') !!}">
            {!! csrf_field() !!}
            <div class="col-md-12 raw-margin-top-24">
                <label>Nama Pengguna</label>
                <input class="form-control" type="text" name="nipp" value="{{ old('nipp') }}">
            </div>
            <div class="col-md-12 raw-margin-top-24">
                <label>Kata Sandi</label>
                <input class="form-control" type="password" name="password" id="password">
            </div>
            <div class="col-md-12 raw-margin-top-24">
                <label>
                    Ingat Saya <input type="checkbox" name="remember">
                </label>
            </div>
            <div class="col-md-12">
              <br />
                <button class="btn btn-success" type="submit">Masuk</button>
                <!-- Dimatiin untuk deploy client/PDAM
                <button class="btn" href="{!! url('password/reset') !!}">Forgot Password</button>
                <button class="btn btn-info" href="{!! url('register') !!}">Register</button>
                -->
            </div>
        </form>
</div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
    </div>

    <script src="{{ url('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/jquery.easing.min.js') }}"></script>
    <script src="{{ url('js/scrollreveal.min.js') }}"></script>
    <script src="{{ url('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ url('js/creative.min.js') }}"></script>

</body>

</html>
