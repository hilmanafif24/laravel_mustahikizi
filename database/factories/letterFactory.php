<?php
/*
|--------------------------------------------------------------------------
| Letter Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Letter::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'letter_no' => 'aut',
		'agenda_no' => 'rerum',
		'draft_no' => 'voluptatum',
		'letterout_no' => 'vitae',
		'letter_date' => '2018-08-21',
		'received_date' => '2018-08-21',
		'input_date' => '2018-08-21 11:40:54',
		'sender' => 'dignissimos',
		'recipient' => 'voluptatem in eaque officiis',
		'subject' => 'mollitia',
		'appendix' => 'illo',
		'body' => 'et enim numquam dignissimos',
		'letterhead' => 'alias',
		'greeting' => 'ab',
		'signature' => 'numquam',
		'has_read' => '1',
		'lt_level_id' => '1',
		'lt_priority_id' => '1',
		'lt_type_id' => '1',
		'lt_name_id' => '1',
		'parent_id' => '1',
		'finishing_date' => '2018-08-21',
		'note' => 'magnam ipsam est accusamus',
		'sender_user_id' => '1',
		'lt_sender_user_id' => '1',
		'locked' => '1',
		'sender_trashed' => '1',
		'sender_trashed_at' => '2018-08-21 11:40:54',
		'letter_out' => '1',
		'is_draft' => '1',
		'edit_date' => '2018-08-21 11:40:54',
    ];
});
