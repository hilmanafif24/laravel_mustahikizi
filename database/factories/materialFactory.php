<?php
/*
|--------------------------------------------------------------------------
| Material Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Material::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'provident',
		'counter' => '1',
		'suffix' => 'quam',
    ];
});
