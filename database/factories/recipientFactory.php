<?php
/*
|--------------------------------------------------------------------------
| Recipient Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Recipient::class, function (Faker\Generator $faker) {
    return [
        '=id' => '1',
		'user_id' => '1',
		'lt_letter_id' => '1',
		'is_cc' => '1',
		'is_checker' => '1',
		'has_been_read' => '1',
		'has_finished' => '1',
		'trashed' => '1',
		'trashed_at' => '2018-08-21 12:24:21',
		'approved' => '1',
		'reading_date' => '2018-08-21 12:24:21',
    ];
});
