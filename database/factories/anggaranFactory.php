<?php
/*
|--------------------------------------------------------------------------
| Anggaran Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Anggaran::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'name' => 'quos',
		'counter' => '1',
		'suffix' => 'corporis',
    ];
});
