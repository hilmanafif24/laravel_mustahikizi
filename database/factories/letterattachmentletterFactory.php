<?php
/*
|--------------------------------------------------------------------------
| Letterattachmentletter Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Letterattachmentletter::class, function (Faker\Generator $faker) {
    return [
        '=lt_attachment_id' => '1',
		'lt_letter_id' => '1',
    ];
});
