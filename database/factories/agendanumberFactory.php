<?php
/*
|--------------------------------------------------------------------------
| Agendanumber Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Agendanumber::class, function (Faker\Generator $faker) {
    return [
        '=id' => '1',
		'name' => 'voluptatibus',
		'counter' => '1',
		'suffix' => 'maxime',
    ];
});
