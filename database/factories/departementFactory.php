<?php
/*
|--------------------------------------------------------------------------
| Departement Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Departement::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'code' => 'assumenda',
		'name' => 'exercitationem',
		' parent_id' => 'sunt',
		' position' => '1',
		'status' => '1',
    ];
});
