<?php
/*
|--------------------------------------------------------------------------
| Jabatan Factory
|--------------------------------------------------------------------------
*/

$factory->define(App\Models\Jabatan::class, function (Faker\Generator $faker) {
    return [
        'id' => '1',
		'kode_jabatan' => 'reprehenderit',
		'name' => 'sint',
		'level' => 'corporis',
		'parent_id' => 'voluptatem',
    ];
});
