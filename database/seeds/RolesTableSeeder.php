<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (! Role::where('name', 'member')->first()) {
            Role::create([
                'name' => 'member',
                'label' => 'Member',
            ]);   
        }

        if (! Role::where('name', 'admin')->first()) {
            Role::create([
                'name' => 'admin',
                'label' => 'Admin',
            ]);  
        }

        if (! Role::where('name', 'admincompany')->first()) {
            Role::create([
                'name' => 'admincompany',
                'label' => 'Admin Company',
            ]); 
        }

        if (! Role::where('name', 'pemberi_perintah')->first()) {
            Role::create([
                'name' => 'pemberi_perintah',
                'label' => 'Manajer Senior, Manajer dan Manajer Junior ',
            ]); 
        }

        if (! Role::where('name', 'pemberi_tugas')->first()) {
            Role::create([
                'name' => 'pemberi_tugas',
                'label' => 'Supervisor',
            ]); 
        }

        if (! Role::where('name', 'pelaksana')->first()) {
            Role::create([
                'name' => 'pelaksana',
                'label' => 'Pegawai PDMA Tirta Raharja',
            ]); 
        }
        if (! Role::where('name', 'pembagi_tugas')->first()) {
            Role::create([
                'name' => 'Pembagi Tugas',
                'label' => 'Supervisor Senior (Dapat Membuat Surat Tugas dan meneruskan Surat Tugas ke Supervisor',
            ]); 
        }

    }
}
