<?php

use App\Models\User;
use App\Models\UserMeta;
use App\Services\UserService;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service = app(UserService::class);

        if (!User::where('name', 'Admin')->first()) {
            $user = User::create([
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'handphone' => '08123456789',
                'password' => bcrypt('admin'),
                'company_id' => '0',
                'nipp' => '1',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'admin', 'admin', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'admin.png']);
        }

        /* create($user, $password, $role = 'member', $sendEmail = true) */

        if (!User::where('name', 'Member')->first()) {
            $user = User::create([
                'name' => 'Member',
                'email' => 'member@member.com',
                'handphone' => '081987654321',
                'password' => bcrypt('member'),
                'company_id' => '1',
                'nipp' => '2',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'member', 'member', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.png']);
        }

        if (!User::where('name', 'AdminCompany')->first()) {
            $user = User::create([
                'name' => 'AdminCompany',
                'email' => 'admincompany@admin.com',
                'handphone' => '081',
                'password' => bcrypt('081'),
                'company_id' => '1',
                'nipp' => '3',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'admincompany', 'admincompany', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'admin.png']);
        }

        if (!User::where('name', 'AdminCompany2')->first()) {
            $user = User::create([
                'name' => 'AdminCompany2',
                'email' => 'admincompany2@admin.com',
                'handphone' => '082',
                'password' => bcrypt('082'),
                'company_id' => '2',
                'nipp' => '4',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'admincompany', 'admincompany', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'admin.png']);
        }

      
        if (!User::where('name', 'Member1')->first()) {
            $user = User::create([
                'name' => 'Member1',
                'email' => 'member1@member.com',
                'handphone' => '08111',
                'password' => bcrypt('08111'),
                'company_id' => '1',
                'nipp' => '5',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'member', 'member', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.png']);
        }

        if (!User::where('name', 'Member2')->first()) {
            $user = User::create([
                'name' => 'Member2',
                'email' => 'member2@member.com',
                'handphone' => '08222',
                'password' => bcrypt('08222'),
                'company_id' => '2',
                'nipp' => '6',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'member', 'member', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.png']);
        }

        if (!User::where('name', 'Pemberi Perintah')->first()) {
            $user = User::create([
                'name' => 'Pemberi Perintah',
                'email' => 'pp@pp.com',
                'handphone' => '100',
                'password' => bcrypt('100'),
                'company_id' => '1',
                'nipp' => '100',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'pemberi_perintah', 'pemberi_perintah', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.png']);
        }

        if (!User::where('name', 'Pemberi Tugas')->first()) {
            $user = User::create([
                'name' => 'Pemberi Tugas',
                'email' => 'pt@pt.com',
                'handphone' => '200',
                'password' => bcrypt('200'),
                'company_id' => '1',
                'nipp' => '200',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'pemberi_tugas', 'pemberi_tugas', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.png']);
        }

        if (!User::where('name', 'Pelaksana')->first()) {
            $user = User::create([
                'name' => 'Pelaksana',
                'email' => 'plt@plt.com',
                'handphone' => '300',
                'password' => bcrypt('300'),
                'company_id' => '1',
                'nipp' => '300',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'pelaksana', 'pelaksana', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.png']);
        }

        if (!User::where('name', 'Pelaksana')->first()) {
            $user = User::create([
                'name' => 'Pelaksana2',
                'email' => 'plt2@plt.com',
                'handphone' => '500',
                'password' => bcrypt('500'),
                'company_id' => '1',
                'nipp' => '500',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'pelaksana', 'pelaksana', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.png']);
        }

        if (!User::where('name', 'Pembagi Tugas')->first()) {
            $user = User::create([
                'name' => 'Pembagi Tugas',
                'email' => 'pm_tugas@pm_tugas.com',
                'handphone' => '400',
                'password' => bcrypt('400'),
                'company_id' => '1',
                'nipp' => '400',
                'departement_id' => '0',
                'jabatan_id' => '0'
            ]);
            $service->create($user, 'pembagi_tugas', 'Pembagi tugas', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.png']);
        }

    }
}
