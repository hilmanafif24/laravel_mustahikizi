<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a given Closure or controller and enjoy the fresh air.
|
*/

/*
|--------------------------------------------------------------------------
| Welcome Page
|--------------------------------------------------------------------------
*/

Route::get('/', 'PagesController@home');

/*
|--------------------------------------------------------------------------
| Login/ Logout/ Password
|--------------------------------------------------------------------------
*/
//Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('login', 'Auth\LoginController@showForm')->name('login');
Route::post('otp', 'Auth\LoginController@otp');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/*
|--------------------------------------------------------------------------
| Registration & Activation
|--------------------------------------------------------------------------
*/
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

Route::get('activate/token/{token}', 'Auth\ActivateController@activate');
Route::group(['middleware' => ['auth']], function () {
    Route::get('activate', 'Auth\ActivateController@showActivate');
    Route::get('activate/send-token', 'Auth\ActivateController@sendToken');
});

/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['auth']], function () {

    /*
    |--------------------------------------------------------------------------
    | Manajer
    |--------------------------------------------------------------------------
    */

     Route::get('/manajer', 'Manajer\ManajerController@index');
     Route::get('/manajer/spk', 'Manajer\ManajerController@spk');
     Route::get('/manajer/createspk', 'Manajer\ManajerController@spk_add');
     //Route::post('/manajer/storespk', 'Manajer\ManajerController@spk_store');
     Route::post('/manajer/storespk', [
        'as' => 'manajer.storespk',
        'uses' => 'Manajer\ManajerController@spk_add'
    ]);

     Route::get('/manajer/spk/{id}', 'Manajer\ManajerController@spk_show');
     Route::get('/manajer/laporan', 'Manajer\ManajerController@laporan');
     Route::get('/manajer/laporan/{id}', 'Manajer\ManajerController@laporan_show');

    /*
    |--------------------------------------------------------------------------
    | Supervisor
    |--------------------------------------------------------------------------
    */

     Route::get('/supervisor', 'Supervisor\SupervisorController@index');
     Route::get('/supervisor/spk', 'Supervisor\SupervisorController@spk');
     Route::get('/supervisor/spk/{id}', 'Supervisor\SupervisorController@spk_show');
     Route::get('/supervisor/surtu', 'Supervisor\SupervisorController@surtu');
     Route::get('/supervisor/surtu/{id}', 'Supervisor\SupervisorController@surtu_show');
     Route::get('/supervisor/createsurtu/{id}', 'Supervisor\SupervisorController@surtu_add');
     Route::post('/supervisor/surtu/store', 'Supervisor\SupervisorController@surtu_store');


     Route::get('/supervisor/laporan', 'Supervisor\SupervisorController@laporan');
     Route::get('/supervisor/laporan/{id}', 'Supervisor\SupervisorController@laporan_show');
     Route::get('/supervisor/createlaporan/{id}', 'Supervisor\SupervisorController@laporan_add');
     Route::post('/supervisor/laporan/store', 'Supervisor\SupervisorController@laporan_store');
     Route::get('/supervisor/laporanpelaksana', 'Supervisor\SupervisorController@laporanpelaksana');
     Route::get('/supervisor/laporanpelaksana/{id}', 'Supervisor\SupervisorController@laporanpelaksana_show');


    /*
    |--------------------------------------------------------------------------
    | Pelaksana
    |--------------------------------------------------------------------------
    */
     Route::get('/pelaksana', 'Pelaksana\PelaksanaController@index');
     Route::get('/pelaksana/surtu', 'Pelaksana\PelaksanaController@surtu');
     Route::get('/pelaksana/surtu/{id}', 'Pelaksana\PelaksanaController@surtu_show');
     Route::get('/pelaksana/laporan', 'Pelaksana\PelaksanaController@laporan');
     Route::get('/pelaksana/laporan/{id}', 'Pelaksana\PelaksanaController@laporan_show');
     Route::get('/pelaksana/createlaporan/{id}', 'Pelaksana\PelaksanaController@laporan_add');

    /*
    |--------------------------------------------------------------------------
    | Letter Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('letters', 'LettersController', ['except' => ['show', 'delete']]);
    Route::post('letters/search', [
        'as' => 'letters.search',
        'uses' => 'LettersController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Recipient Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('recipients', 'RecipientsController', ['except' => ['show', 'delete']]);
    Route::post('recipients/search', [
        'as' => 'recipients.search',
        'uses' => 'RecipientsController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Lettertype Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('lettertypes', 'LettertypesController', ['except' => ['show', 'delete']]);
    Route::post('lettertypes/search', [
        'as' => 'lettertypes.search',
        'uses' => 'LettertypesController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Letterattachment Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('letterattachments', 'LetterattachmentsController', ['except' => ['show', 'delete']]);
    Route::post('letterattachments/search', [
        'as' => 'letterattachments.search',
        'uses' => 'LetterattachmentsController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Lettertype Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('lettertypes', 'LettertypesController', ['except' => ['show', 'delete']]);
    Route::post('lettertypes/search', [
        'as' => 'lettertypes.search',
        'uses' => 'LettertypesController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Letterattachment Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('letterattachments', 'LetterattachmentsController', ['except' => ['show', 'delete']]);
    Route::post('letterattachments/search', [
        'as' => 'letterattachments.search',
        'uses' => 'LetterattachmentsController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Letterattachmentletter Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('letterattachmentletters', 'LetterattachmentlettersController', ['except' => ['show', 'delete']]);
    Route::post('letterattachmentletters/search', [
        'as' => 'letterattachmentletters.search',
        'uses' => 'LetterattachmentlettersController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Agendanumber Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('agendanumbers', 'AgendanumbersController', ['except' => ['show', 'delete']]);
    Route::post('agendanumbers/search', [
        'as' => 'agendanumbers.search',
        'uses' => 'AgendanumbersController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Agendanumberuser Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('agendanumberusers', 'AgendanumberusersController', ['except' => ['show', 'delete']]);
    Route::post('agendanumberusers/search', [
        'as' => 'agendanumberusers.search',
        'uses' => 'AgendanumberusersController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Jabatan Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('jabatans', 'JabatansController', ['except' => ['show', 'delete']]);
    Route::post('jabatans/search', [
        'as' => 'jabatans.search',
        'uses' => 'JabatansController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Departement Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('departements', 'DepartementsController', ['except' => ['show', 'delete']]);
    Route::post('departements/search', [
        'as' => 'departements.search',
        'uses' => 'DepartementsController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Lettersource Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('lettersources', 'LettersourcesController', ['except' => ['show', 'delete']]);
    Route::post('lettersources/search', [
        'as' => 'lettersources.search',
        'uses' => 'LettersourcesController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Anggaran Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('anggarans', 'AnggaransController', ['except' => ['show', 'delete']]);
    Route::post('anggarans/search', [
        'as' => 'anggarans.search',
        'uses' => 'AnggaransController@search'
    ]);

    /*
    |--------------------------------------------------------------------------
    | Material Routes
    |--------------------------------------------------------------------------
    */

    Route::resource('materials', 'MaterialsController', ['except' => ['show', 'delete']]);
    Route::post('materials/search', [
        'as' => 'materials.search',
        'uses' => 'MaterialsController@search'
    ]);


    /*
    |--------------------------------------------------------------------------
    | General
    |--------------------------------------------------------------------------
    */

    Route::get('/users/switch-back', 'Admin\UserController@switchUserBack');

    /*
    |--------------------------------------------------------------------------
    | User
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
        Route::get('settings', 'SettingsController@settings');
        Route::post('settings', 'SettingsController@update');
        Route::get('password', 'PasswordController@password');
        Route::post('password', 'PasswordController@update');
    });

    /*
    |--------------------------------------------------------------------------
    | Dashboard
    |--------------------------------------------------------------------------
    */

    Route::get('/dashboard', function(){ return Redirect::to('cms/dashboard'); });

    

    /*
    |--------------------------------------------------------------------------
    | Admin
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {

        Route::get('dashboard', 'DashboardController@index');

        /*
        |--------------------------------------------------------------------------
        | Users
        |--------------------------------------------------------------------------
        */
        Route::resource('users', 'UserController', ['except' => ['show']]);
        Route::post('users/search', 'UserController@search');
        Route::get('users/search', 'UserController@index');
        Route::get('users/invite', 'UserController@getInvite');
        Route::get('users/switch/{id}', 'UserController@switchToUser');
        Route::post('users/invite', 'UserController@postInvite');
        Route::post('users/store', 'UserController@store');
        Route::post('users/searchcompany', 'UserController@searchcompany');
        Route::get('users/upload', 'UserController@upload');
        Route::post('users/import', 'UserController@import');

        /*
        |--------------------------------------------------------------------------
        | Roles
        |--------------------------------------------------------------------------
        */
        Route::resource('roles', 'RoleController', ['except' => ['show']]);
        Route::post('roles/search', 'RoleController@search');
        Route::get('roles/search', 'RoleController@index');
        /*
        |--------------------------------------------------------------------------
        | Company Routes
        |--------------------------------------------------------------------------
        */
        Route::resource('companies', 'CompaniesController', ['except' => ['show']]);
        Route::post('companies/search', [
            'as' => 'companies.search',
            'uses' => 'CompaniesController@search'
        ]);
    });
});

